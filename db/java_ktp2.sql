-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 10:36 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `java_ktp2`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(83);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `gol` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `kec` varchar(255) DEFAULT NULL,
  `kel` varchar(255) DEFAULT NULL,
  `kwr` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nik` int(11) NOT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `rt` varchar(255) DEFAULT NULL,
  `rw` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `agama`, `alamat`, `gol`, `jk`, `kec`, `kel`, `kwr`, `name`, `nik`, `pekerjaan`, `rt`, `rw`, `status`, `tanggal`, `tempat`) VALUES
(59, 'Islam', 'jkt', 'A', 'P', 'kalideres', 'kalideres', 'WNI', 'saya', 1092381023, 'hacker', '09', '01', 'jomblo', '', 'jakarta'),
(63, 'islam', 'Jakarta', 'A', 'L', 'kamal', 'kamal', 'WNI', 'bejo', 920813921, 'hakel', '09', '09', 'Jomblo', '2001-09-09', 'Jakarta'),
(76, 'islam', 'Jakarta Barat', 'AB', 'L', 'Kamal', 'Kamal', 'WNI', 'jalu-jalu', 44142321, 'Pemburu', '09', '09', 'Menikah', '2001-09-09', 'Jakarta'),
(79, 'islam', 'jakarta', 'A', 'L', 'kamal', 'kamal', 'WNI', 'udinzzz', 929391293, 'hakel', '09', '09', 'jomblo', '2002-09-09', 'Jakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
