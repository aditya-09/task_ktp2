const url = "http://localhost:8181/"
var detail = 'http://127.0.0.1:5501/detail-user.html?nik'

function getData(url) {
  let xmlhttp = new XMLHttpRequest()
  xmlhttp.onreadystatechange = function () {
      if(this.readyState == 4 && this.status == 200){
          let resp = JSON.parse(this.responseText)
          let myResp = ""
          let modal = ""
          for (let i = 0; i < resp.length; i++) {
              if (resp[i].jk === "L"){
                myResp += `
                            <div class="col-md-3 mt-5">
                              <a href="" style="text-decoration: none; color: black;" data-toggle="modal" data-target="#`+resp[i].name+`">
                                  <div class="card border-info ">
                                      <div class="text-center mt-3">
                                          <img src="./img/man.png" style="width: 180px !important;" class="img-fluid">
                                      </div>
                                      <div class="card-body border-bottom border-info">
                                        <h5 class="card-title">`+resp[i].name+`</h5>
                                        <span class="" style="font-size: 14px;">`+resp[i].nik+`</span>
                                      </div>
                                      <div class="card-body text-center">
                                            <a href="detail-user.html?id=`+resp[i].id+`"><button class="mt-2 btn btn-outline-info mr-2">Cek Data</button></a>
                                            <a href="" onclick="deleteId(`+resp[i].id+`);"><button class="mt-2 btn btn-outline-danger ml-2">Hapus</button></a>
                                        </div>
                                  </div>
                              </a>
                          </div>
                `
              }else if (resp[i].jk === "P"){
                myResp += `
                            <div class="col-md-3 mt-5">
                              <a href="" style="text-decoration: none; color: black;" data-toggle="modal" data-target="#`+resp[i].name+`">
                                  <div class="card border-info ">
                                      <div class="text-center mt-3">
                                          <img src="./img/woman.png" style="width: 180px !important;" class="img-fluid">
                                      </div>
                                      <div class="card-body border-bottom border-info">
                                        <h5 class="card-title">`+resp[i].name+`</h5>
                                        <span class="" style="font-size: 14px;">`+resp[i].nik+`</span>
                                      </div>
                                      <div class="card-body text-center">
                                            <a href=detail-user.html?id=`+resp[i].id+`""><button class="mt-2 btn btn-outline-info mr-2">Cek Data</button></a>
                                            <a href="" onclick="deleteId(`+resp[i].id+`);"><button class="mt-2 btn btn-outline-danger ml-2">Hapus</button></a>
                                        </div>
                                  </div>
                              </a>
                          </div>
                `
              }else{
                myResp += `
                <div class="col-md-3 mt-5">
                  <a href="" style="text-decoration: none; color: black;" data-toggle="modal" data-target="#`+resp[i].name+`">
                      <div class="card border-info ">
                          <div class="text-center mt-3">
                              <img src="./img/anonymous (1).png" style="width: 180px !important;" class="img-fluid">
                          </div>
                          <div class="card-body border-bottom border-info">
                            <h5 class="card-title">`+resp[i].name+`</h5>
                            <span class="" style="font-size: 14px;">`+resp[i].nik+`</span>
                          </div>
                          <div class="card-body text-center">
                                <a href="detail-user.html?id=`+resp[i].id+`"><button class="mt-2 btn btn-outline-info mr-2">Cek Data</button></a>
                                <a href="" onclick="deleteId(`+resp[i].id+`);"><button class="mt-2 btn btn-outline-danger ml-2">Hapus</button></a>
                            </div>
                      </div>
                  </a>
              </div>
    `
              }

              modal += `
              <div class="modal fade" id="`+resp[i].name+`" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Modal User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                      <div class="row mt-4">
                          <div class="col-md-6">
                          <div class="table-responsive">
                              <table class="table table-borderless table-light">
                                  <thead>
                                      <tr>
                                          <th scope="col">N I K</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Nama</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Gol. Darah</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">TTL</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">JK</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Agama</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Status</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Pekerjaan</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Kewarganegaraan</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Alamat</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">RT</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">RW</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Kelurahan</th>
                                      </tr>
                                      <tr>
                                          <th scope="col">Kecamatan</th>
                                      </tr>
                                  </thead>
                              </table>
                          </div>
                          </div>
                          <div class="col-md-6">
                          <div class="table-responsive">
                              <table class="table table-borderless table-light">
                                  <thead id="where_user">
                                    <tr>
                                        <th scope="col">`+resp[i].name+`</th>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].nik+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].gol+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].tempat+`, `+resp[i].tanggal+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].jk+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].agama+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].status+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].pekerjaan+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].kwr+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].alamat+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].rt+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].rw+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].kel+`</td>
                                    </tr>
                                    <tr>
                                        <td>`+resp[i].kec+`</td>
                                    </tr>
                                  </thead>
                              </table>
                          </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                  <a href="detail-user.html?id=`+resp[i].id+`" class="btn btn-info"> Cek Data </a>
                  <button class="btn btn-danger" onclick="deleteId(`+resp[i].id+`);">Delete</button>
                  <a href="update.html?id=`+resp[i].id+`" class="btn btn-success"> Update Data </a>
                  </div>
              </div>
              </div>
          </div>
              `
          }
          document.getElementById("dataUser").innerHTML = myResp
          document.getElementById("nav-tabContent").innerHTML = modal

      }
  }

  xmlhttp.open("GET", url,true)
  xmlhttp.send()
}

function addData() {
  var nama = document.getElementById("nama").value
  var nik = document.getElementById("nik").value
  var tempat = document.getElementById("tempat").value
  var tanggal = document.getElementById("tgl").value
  var jk = document.getElementById("jk").value
  var alamat = document.getElementById("alamat").value
  var rt = document.getElementById("rt").value
  var rw = document.getElementById("rw").value
  var kel = document.getElementById("kel").value
  var kec = document.getElementById("kec").value
  var agama = document.getElementById("agama").value
  var pekerjaan = document.getElementById("pekerjaan").value
  var kwr = document.getElementById("kwr").value
  var status = document.getElementById("status").value
  var gol = document.getElementById("gol").value


    var str_json = JSON.stringify({
        nik: nik,
        name: nama,
        tempat: tempat,
        tanggal: tanggal,
        jk: jk,
        alamat: alamat,
        rt: rt,
        rw: rw,
        kel: kel,
        kec: kec,
        agama: agama,
        pekerjaan: pekerjaan,
        gol : gol,
        kwr : kwr,
        status : status
    })
    console.log(str_json)
    fetch('http://localhost:8181/addUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: str_json,
        })
        // .then(response => response.json())
        // .then(data => {
        //     alert("berhasil")
        //     console.log('Success:', data);
        //     window.location.href = "index.html"
        // })
        .catch((error) => {
            console.error('Error:', error);
        });

    // location.reload()
}

// function btnSelete_Click() {
//     var strconfirm = confirm("Are you sure you want to delete?");
//     if (strconfirm == true) {
//         return true;
//     }
// }

function deleteId (id) {
    console.log();
    var strconfirm = confirm("Are you sure you want to delete?");
    if (strconfirm == true) {
        fetch(`${url}/delete/${id}`, {
            method: "DELETE",
        })
        .then(res => location.reload())
    }
    

};
// getData('http://localhost:8181/users')

function getItemById(id) {
    fetch(`http://localhost:8181/user/`+id)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
            
}
const renderItemsById = (datas) =>{
    let myResp = `
        <tr>
            <th scope="row">`+datas.name+`</th>
            <td>`+datas.nik+`</td>
            <td>`+datas.gol+`</td>
            <td>`+datas.tempat+`, `+datas.tanggal+`</td>
            <td>`+datas.jk+`</td>
            <td>`+datas.agama+`</td>
            <td>`+datas.status+`</td>
            <td>`+datas.pekerjaan+`</td>
            <td>`+datas.kwr+`</td>
            <td>`+datas.alamat+`</td>
            <td>`+datas.rt+`</td>
            <td>`+datas.rw+`</td>
            <td>`+datas.kel+`</td>
            <td>`+datas.kec+`</td>
            <td><a href="index.html" class="btn btn-secondary">Kembali</a></td>
        </tr>
    
    `
    document.getElementById("whereData").innerHTML = myResp

}