const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const idParams = urlParams.get('id')
console.log(idParams);

function getItemById() {
    fetch(`http://localhost:8181/user/` + idParams)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
}
const renderItemsById = (data) => {
    let myResp = `
    <div class="offset-s1 col s10">
    <div class="text center mt">
        <img src="./img/user.png" width="100px" alt="">
    </div>
    <div class="input-field col s12">
        <input id="nama" type="text" class="validate"  placeholder="Nama" value="${data.name}">
    </div>
    <div class="input-field col s6">
        <input id="nik" type="text" class="validate"  placeholder="N I K" value="${data.nik}">
    </div>
    <div class="input-field col s6">
        <input id="gol" type="text" class="validate"  placeholder="Gol. Darah" value="${data.gol}">
    </div>
    <div class="input-field col s2">
        <input id="tempat" type="text" class="validate" placeholder="Tempat" value="${data.tempat}">
    </div>
    <div class="input-field col s4">
        <input id="tgl" type="date" class="datepicker" value="${data.tanggal}">
    </div>
    <div class="input-field col s6">
        <input id="jk" type="text" class="validate"  placeholder="Jenis Kelamin" value="${data.jk}">
    </div>
    <div class="input-field col s6">
        <input id="agama" type="text" class="validate"  placeholder="Agama" value="${data.agama}">
    </div>
    <div class="input-field col s6">
        <input id="status" type="text" class="validate"  placeholder="Status" value="${data.status}">
    </div>
    <div class="input-field col s6">
        <input id="pekerjaan" type="text" class="validate"  placeholder="Pekerjaan" value="${data.pekerjaan}">
    </div>
    <div class="input-field col s6">
        <input id="kwr" type="text" class="validate"  placeholder="Kewarganegaraan" value="${data.kwr}" >
    </div>
    <div class="input-field col s12">
        <textarea id="alamat" class="materialize-textarea" placeholder="alamat">${data.alamat}</textarea>
    </div>
    <div class="input-field col s2">
        <input id="rt" type="text" class="validate"  placeholder="RT" value="${data.rt}">
    </div>
    <div class="input-field col s2">
        <input id="rw" type="text" class="validate"  placeholder="RW" value="${data.rw}">
    </div>
    <div class="input-field col s4">
        <input id="kel" type="text" class="validate"  placeholder="Kelurahan" value="${data.kel}">
    </div>
    <div class="input-field col s4">
        <input id="kec" type="text" class="validate"  placeholder="Kecamatan" value="${data.kec}">
    </div>

    <div class="col s12">
        <button class="btn waves-effect waves-light" onclick="update()" id="kirim" type="submit"
            name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </div>
</div>
    
    `
    document.getElementById("content").innerHTML = myResp
}

function update(id) {
    var nik = document.getElementById("nik").value
    var nama = document.getElementById("nama").value
    var tempat = document.getElementById("tempat").value
    var tanggal = document.getElementById("tgl").value
    var jk = document.getElementById("jk").value
    var alamat = document.getElementById("alamat").value
    var rt = document.getElementById("rt").value
    var rw = document.getElementById("rw").value
    var kel = document.getElementById("kel").value
    var kec = document.getElementById("kec").value
    var agama = document.getElementById("agama").value
    var pekerjaan = document.getElementById("pekerjaan").value
    var kwr = document.getElementById("kwr").value
    var status = document.getElementById("status").value
    var gol = document.getElementById("gol").value


    var str_json = JSON.stringify({
        nik: nik,
        name: nama,
        tempat: tempat,
        tanggal: tanggal,
        jk: jk,
        alamat: alamat,
        rt: rt,
        rw: rw,
        kel: kel,
        kec: kec,
        agama: agama,
        pekerjaan: pekerjaan,
        gol: gol,
        kwr: kwr,
        status: status
    })
    console.log(str_json)
    fetch('http://localhost:8181/update/' + idParams, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: str_json,
        })
        // .then(response => response.json())
        .then(data => {
            alert("berhasil")
            console.log('Success:', data);
            window.location.href = "index.html"
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    // window.location.href = "index.html"
}

getItemById()